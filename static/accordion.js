$(document).ready(() => {
    $(".accordion-bar").on('click', event => {
        if ($(event.target).is('button, img')) {
            event.preventDefault();
            return;
        }
        $(event.currentTarget).next().slideToggle();
        const $other_accordions = $(event.currentTarget).parent().siblings();

        $other_accordions.each((index, $accordion) => {
            $($accordion).find('.accordion-content').slideUp();
        });
    });

    $('.down').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        $($accordion).before($($accordion).next());
    });

    $('.up').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        $($accordion).after($($accordion).prev());
    });

    $('#style-modifier').on('click', () => {
        $('.accordion-bar').toggleClass('new-bar');
        $('.accordion').toggleClass('new-accordion')
        $('body').toggleClass('new-body');
        $('.buttons button').toggleClass('new-button')
    });
});