$(document).ready(() => {
    get_books('taubat');

    $('#search-box').keypress(event => { 
        if(event.which == 13) { // enter key
            $('#search-button').click(); 
        }
    });

    $('#search-button').on('click', event => {
        let keyword = $('#search-box').val();
        if(keyword == '') keyword = 'taubat';
        get_books(keyword);
    });

    $('.modal-button').on('click', () => {
        $('#modal-table').hide();
        $('#modal-loader').show();
        $('.modal-data').remove();

        $.ajax({
            type: "GET",
            url: get_top5_url,
            success: response => {
                response.forEach(book_info => {
                    const book = book_info.fields;
                    $('#modal-table').append(
                        $('<tr/>').addClass('modal-data').append(
                            $('<td/>').append(
                                $('<img/>').attr('src', book.cover)
                            )
                        ).append(
                            $('<td/>').html(book.title)
                        ).append(
                            $('<td/>').html(book.author)
                        ).append(
                            $('<td/>').html(book.publisher)
                        ).append(
                            $('<td/>').html(book.likes)
                        )
                    )
                });
                $('#modal-loader').hide();
                $('#modal-table').show();
            }
        });
    });
    
    function get_author(book_info) {
        if(book_info.authors) {
            let author_str = "";
            book_info.authors.forEach(author => {
                author_str += ', ' + author
            });
            return author_str.slice(2);
        } else return "unknown"
    }

    function add_likes(likes_count_p, book, amount) {
        book['amount'] = amount;
        $.ajax({
            type: "POST",
            url: add_likes_url,
            data: book,
            success: response => {
                let likes_count = parseInt(likes_count_p.val());
                let stored_likes_count = parseInt(response);
                
                if(likes_count < stored_likes_count) {
                    likes_count_p.html(stored_likes_count);
                }
            }
        });
    }

    function get_books(keyword) {
        $('.data').remove();
        $('.not-found').hide();
        $('#main-table').hide();
        $('#loader').show();
        $.ajax({
            type: "GET",
            url: get_books_url + keyword,
            success: result => {
                if(!result.items) {
                    $('.not-found').show();
                } else {
                    result.items.forEach(item => {
                        const book_info = item.volumeInfo;
                        const book = {
                            id: item.id,
                            cover: () => {
                                if(book_info.imageLinks) {
                                    return book_info.imageLinks.smallThumbnail;
                                }
                                return default_cover;
                            },
                            title: book_info.title,
                            author: get_author(book_info),
                            publisher: (book_info.publisher || "unknown"),
                            likes: book_info.likes
                        }
                        $('#main-table').append(
                            $('<tr/>').addClass('data').attr('id', book.id).append(
                                $('<td/>').addClass('cover').append(
                                    $('<img/>').attr('src', book.cover).addClass('book-cover')
                                )
                            ).append(
                                $('<td/>').addClass('title').html(book.title)
                            ).append(
                                $('<td/>').addClass('author').html(book.author)
                            ).append(
                                $('<td/>').addClass('publisher').html(book.publisher)
                            ).append(
                                $('<td/>').addClass('action').append(
                                    $('<button/>').addClass('like-button').attr('data-id', book.id).append(
                                        $('<img/>').attr('src', thumblike).addClass('thumblike')
                                    ).on('click', event => {
                                        let like_count = $(event.currentTarget).next();
                                        let likes = like_count.html();
                                        $(like_count).html(parseInt(likes) + 1);
                                        add_likes(like_count, book, 1);
                                    })
                                ).append(
                                    $('<p/>').addClass('like-count').html(book.likes)
                                )
                            )
                        );
                    });
                }
                $('#loader').hide();
                $('#main-table').show();
            }
        });
    }
});
