from django.test import TestCase
from django.urls import resolve
from django.contrib.auth.models import User


class UnitTest(TestCase):

    def setUp(self):
        User.objects.create_user(username="test_user", password="test_user")

    def test_cant_access_homepage_without_login(self):
        response = self.client.get('/story10/home')
        self.assertEqual(response.status_code, 302)
        self.assertIn('/story10/login', response.url)

    def test_can_logout(self):
        self.client.login(username="test_user", password="test_user")
        response = self.client.get('/story10/logout')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/story10/home')
        self.assertIn('/story10/login', response.url)

    def test_cant_login_if_still_logged_in(self):
        self.client.login(username="test_user", password="test_user")
        response = self.client.get('/story10/login')
        
        self.assertEqual(response.status_code, 302)
        self.assertNotIn('/story10/login', response.url)
    
    def test_cant_signup_if_still_logged_in(self):
        self.client.login(username="test_user", password="test_user")
        response = self.client.get('/story10/signup')

        self.assertEqual(response.status_code, 302)
        self.assertNotIn('/story10/signup', response.url)
