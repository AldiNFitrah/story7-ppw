from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.selenium = webdriver.Chrome(
            './chromedriver',
            chrome_options=chrome_options
        )
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_can_signup_login_logout(self):
        # Lathi heard a new site that can simulate a login system
        # She's so bored during the quarantine, so why doesnt she give it a try
        self.selenium.get(self.live_server_url + '/story10') # FINALLY STORY 10
        
        # There are 2 buttons on the screen, inscribed with SignUp and Login
        # Because she hasn't have an account yet, she clicks the SignUp button
        signup_button = self.selenium.find_element_by_id('signup-button')
        signup_button.click()

        # Now she's redirected to a signup page
        # And has to fill some form
        email_form = self.selenium.find_element_by_id('email')
        photo_form = self.selenium.find_element_by_id('photo')
        username_form = self.selenium.find_element_by_id('username')
        password_form = self.selenium.find_element_by_id('password')

        email_form.send_keys('lathi@genius.com')
        username_form.send_keys('lathi_wgenius')
        photo_form.send_keys("https://i.ibb.co/mz2hjZy/penguitten.png")
        password_form.send_keys('ini/lagunya/enak')

        # And clicks the signup button
        signup_submit = self.selenium.find_element_by_id('signup-submit')
        signup_submit.click()
        alert = self.selenium.switch_to.alert
        alert.accept()

        # After that, she's redirected to the login page
        # So she fills the form up
        username_form = self.selenium.find_element_by_id('username')
        password_form = self.selenium.find_element_by_id('password')

        username_form.send_keys('lathi_wgenius')
        password_form.send_keys('ini/lagunya/enak')

        # And clicks the login button
        login_submit = self.selenium.find_element_by_id('login-submit')
        login_submit.click()

        # Again, she's redirected, but now to the welcome page
        # There, she sees a greetings with her username
        # Her email and profile pic also displayed on the page
        page_text = self.selenium.find_element_by_tag_name('body').text

        self.assertIn('lathi_wgenius', page_text)
        self.assertIn('lathi@genius.com', page_text)

        photo = self.selenium.find_element_by_tag_name('img')
        photo_src = photo.get_attribute('src')
        
        self.assertEqual(photo_src, 'https://i.ibb.co/mz2hjZy/penguitten.png')
