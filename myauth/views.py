from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from myauth.models import MyUser


def index(request):
    if(request.user.is_authenticated):
        return redirect(reverse('myauth:home'))
    
    return render(request, 'myauth.html')

def sign_up(request):
    if(request.user.is_authenticated):
        return redirect(reverse('myauth:home'))

    if(request.method == "GET"):
        return render(request, 'signup.html')
    
    else:
        email = request.POST.get("email")
        photo = request.POST.get("photo")
        username = request.POST.get("username")
        password = request.POST.get("password")

        if(any(not item for item in [email, photo, username, password])):
            messages.error(request, "Datanya belum lengkap pak :(")
            return render(request, 'signup.html')

        try:
            user = User.objects.get(username=username)
            messages.error(request, "usernamenya udah dipake ternyata")
            return render(request, 'signup.html')

        except:
            user = User.objects.create_user(username=username, email=email, password=password)
            my_user = MyUser.objects.create(user=user, photo=photo)

            messages.success(request, "Akun berhasil dibuat")
            return redirect(reverse('myauth:login'))

def log_in(request):
    if(request.user.is_authenticated):
        return redirect(reverse('myauth:home'))

    if(request.method == "GET"):
        return render(request, 'login.html')
    
    else:
        username = request.POST.get("username")
        password = request.POST.get("password")

        # kalo data belum lengkap
        if(not username or not password):
            messages.error(request, "Datanya belum lengkap pak :(")
            return render(request, 'login.html')
        
        # kalo berhasil
        user = authenticate(username=username, password=password)
        if(user is not None):
            login(request, user)
            return redirect(reverse('myauth:home'))

        # kalo ga authenticated
        messages.error(request, "Username atau passwordnya salah tuh")
        return render(request, 'login.html')

def log_out(request):
    logout(request)
    return redirect(reverse('myauth:login'))
    
@login_required(login_url='myauth:login')
def home(request):
    return render(request, 'home.html')
