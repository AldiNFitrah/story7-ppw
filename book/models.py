from django.db import models


class Book(models.Model):
    id = models.CharField(max_length=127, primary_key=True)
    cover = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True)
    publisher = models.CharField(max_length=255, blank=True)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return f"[{self.likes}]|{self.title} - {self.author}"
