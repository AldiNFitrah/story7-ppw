from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.selenium = webdriver.Chrome(
            './chromedriver',
            chrome_options=chrome_options
        )
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_can_search_book_and_see_it_on_best_books_list(self):
        # Locky just discovered a cool website, similar to google book apis, but with more fancy displays 
        # He comes to take a look to the site
        self.selenium.get(self.live_server_url + '/story9')
        self.selenium.implicitly_wait(9)

        # The site named Bookybase. Pretty sure the developer could find another better name.
        self.assertIn("Bookybase", self.selenium.title)

        # There he sees a search box
        search_box = self.selenium.find_element_by_id('search-box')

        # He tries to search a book titled Finale
        # The book is authored by Stephanie Garber
        search_box.send_keys('Finale')
        search_box.send_keys(Keys.ENTER)
        time.sleep(3)

        # After waiting the search being processed, something popped up on the site
        # There he can see the book's brief description he wanted, alongside with the cover
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertIn("Finale", page_text)
        self.assertIn("Stephanie Garber", page_text)

        # Because he unconditionally likes the book, he also wants the book become the top books in this site
        # So he hits the like button
        # Fyi, the book's id in googleapi is ybBrDwAAQBAJ
        fav_book_row = self.selenium.find_element_by_xpath("//tr[@id='ybBrDwAAQBAJ']")
        like_count = fav_book_row.find_element_by_class_name('like-count').text
        like_button = fav_book_row.find_element_by_class_name('like-button')
        like_button.click()

        # Now, the like-counter is increased
        new_like_count = fav_book_row.find_element_by_class_name('like-count').text
        self.assertGreater(int(new_like_count), int(like_count))

        # To ensure that the likes has been counted, Locky tries to refresh the site
        # And see that the likes count still the same
        self.selenium.refresh()
        search_box = self.selenium.find_element_by_id('search-box')
        search_box.send_keys('Finale')
        search_box.send_keys(Keys.ENTER)
        time.sleep(3)
        refreshed_like_count = self.selenium.find_element_by_xpath("//tr[@id='ybBrDwAAQBAJ']//*[@class='like-count']").text
        self.assertEqual(new_like_count, refreshed_like_count)

        # Fortunately this site also have a top list of book
        # It will list top 5 books with the most likes
        # Just to ensure that his fav book included in the top list, He press the like button some more times
        fav_book_row = self.selenium.find_element_by_xpath("//tr[@id='ybBrDwAAQBAJ']")
        like_button = fav_book_row.find_element_by_class_name('like-button')
        for i in range(5):
            like_button.click()
        
        # Now he definitely will see his fav book in the top list
        modal_button = self.selenium.find_element_by_class_name('modal-button')
        modal_button.click()
        time.sleep(3)
        modal_content = self.selenium.find_element_by_class_name('modal-content').text

        self.assertIn("Finale", modal_content)
        self.assertIn("Stephanie Garber", modal_content)
