from django.shortcuts import render
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
import requests

from book.models import Book


def index(request):
    return render(request, 'book.html')

def get_books(request):
    if(request.is_ajax()):
        response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['keyword'])
        data = response.json()

        for item in data['items']:
            try:
                book = Book.objects.get(id=item['id'])
                likes = book.likes
                
            except:
                likes = 0

            item['volumeInfo']['likes'] = likes 

        return JsonResponse(data)

    else:
        return HttpResponseForbidden()

@csrf_exempt
def add_likes(request):
    if(request.is_ajax()):
        book, created = Book.objects.get_or_create(
            id=request.POST['id'],
            cover=request.POST['cover'],
            title=request.POST['title'],
            author=request.POST['author'],
            publisher=request.POST['publisher'],
        )
        book.likes += int(request.POST['amount'])
        book.save()
        return JsonResponse(book.likes, safe=False)
        
    else:
        return HttpResponseForbidden()

def get_top5(request):
    if(request.is_ajax()):
        books = Book.objects.all().order_by('-likes')[:5]
        book_list = serializers.serialize('json', books)
        return HttpResponse(book_list, content_type="text/json-comment-filtered")
        
    else:
        return HttpResponseForbidden()
