from django.urls import path

from book import views


app_name = 'book'

urlpatterns = [
    path('', views.index, name='index'),
    path('get', views.get_books, name='get_books'),
    path('add-likes', views.add_likes, name='add_likes'),
    path('get-top5', views.get_top5, name='get_top5'),
]