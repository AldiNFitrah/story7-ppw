from django.apps import AppConfig


class PewforumConfig(AppConfig):
    name = 'pewforum'
