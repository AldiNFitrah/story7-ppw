from django.test import TestCase
from django.urls import resolve

from pewforum.models import Status
from pewforum.views import change_color


class UnitTest(TestCase):
    def test_get_confirmation_page(self):
        response = self.client.get('/story7/confirm')
        self.assertEqual(response.status_code, 405)

    def test_post_confirmation_page(self):
        response = self.client.post('/story7/confirm', data={"dummy": "dummy"})
        self.assertEqual(response.status_code, 200)

    def test_url_for_change_color(self):
        Status.objects.create(
            author="Najwa Shihab",
            topic="Koruptor masa bebas?",
            color="#123456"
        )
        response = resolve('/story7/change-color/1')
        self.assertEqual(response.func, change_color)
