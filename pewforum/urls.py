from django.urls import path

from pewforum import views


app_name = 'pewforum'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirm', views.confirm, name='confirm'),
    path('change-color/<int:id>', views.change_color, name='change_color')
]