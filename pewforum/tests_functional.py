from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.selenium = webdriver.Chrome(
            './chromedriver',
            chrome_options=chrome_options
        )
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_can_post_status_and_appear_on_homepage(self):
        # Tina just read a news that mentioned a new web-based social media
        # She goes to the web
        self.selenium.get(self.live_server_url + '/story7')
        self.selenium.implicitly_wait(10)

        # The app named enViralle. She recognized it by the webpage's title.
        self.assertIn("enViralle", self.selenium.title)

        # Tina also wants her thoughts appear on the website.
        # She looks at a simple form. She is asked to enter her name
        form_name = self.selenium.find_element_by_id('id_name')
        self.assertEqual(
            form_name.get_attribute('placeholder'),
            "Enter your name"
        )

        # And to write her thought
        form_status = self.selenium.find_element_by_id('id_status')
        self.assertEqual(
            form_status.get_attribute('placeholder'),
            "What do you want to share?"
        )

        # So now, she types her fullname, "Karan Tina"
        form_name.send_keys("Karan Tina")
        
        # And also her thought on the form
        form_status.send_keys("PJJ itu capek :(")

        # She gets directed to a confirmation page when she hits enter
        form_name.send_keys(Keys.ENTER)
        self.assertEqual("Confirmation page", self.selenium.title)

        # She reviews what just she typed
        # In case there's any typo she made
        form_name = self.selenium.find_element_by_id('id_name')
        self.assertEqual(
            form_name.get_attribute('value'),
            "Karan Tina"
        )

        # She also check her status, so she won't get bullied by netizens if there's any mistake in her writing
        form_status = self.selenium.find_element_by_id('id_status')
        self.assertEqual(
            form_status.get_attribute('value'),
            "PJJ itu capek :("
        )
        
        # She clicks yes, and get redirected back to the homepage
        button_yes = self.selenium.find_element_by_id('yes')
        button_yes.click()
        self.assertEqual("enViralle", self.selenium.title)

        # Now she sees her writing there along with her name
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertIn("Karan Tina", page_text)
        self.assertIn("PJJ itu capek :(", page_text)

        # Oh wow, she recognized there's an update on the website, because of challenge story 7
        # Now, all the status are decorated with a background color
        status_container = self.selenium.find_element_by_id('status-wrapper')
        current_color = status_container.value_of_css_property('background-color')

        # The update says she also can change the color of the status displayed in the homepage by clicking the status
        status_container.click()

        # And now she sees that the color has changed
        new_status_container = self.selenium.find_element_by_id('status-wrapper')
        new_color = new_status_container.value_of_css_property('background-color')
        
        self.assertNotEqual(current_color, new_color)
