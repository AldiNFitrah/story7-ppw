from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
import random

from pewforum.models import Status


def get_random_color_hex():
    r = lambda: random.randint(0, 255)
    return f"#{r():02X}{r():02X}{r():02X}"

def index(request):
    context = {}
    if(request.method == "POST"):
        Status.objects.create(
            author = request.POST.get('name'),
            topic = request.POST.get('status'),
            color = get_random_color_hex(),
        )
    
    context = {
        "datas": Status.objects.all()
    }
    return render(request, "pewforum.html", context)

def confirm(request):
    if(request.method == "GET"):
        return HttpResponse(status=405)
    
    elif(request.method == "POST"):
        context  = {
            'name' : request.POST.get('name'),
            'status' : request.POST.get('status')
        }
        return render(request, "confirm.html", context  )

def change_color(request, id):
    data = get_object_or_404(Status, id=id)
    new_color = data.color
    while(new_color == data.color):
        new_color = get_random_color_hex()

    data.color = new_color
    data.save()
    return redirect('/story7')
