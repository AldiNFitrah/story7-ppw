from django.db import models


class Status(models.Model):
    author = models.CharField(max_length=255)
    topic = models.TextField()
    color = models.CharField(max_length=15)
