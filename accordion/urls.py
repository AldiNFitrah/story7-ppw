from django.urls import path

from accordion import views


app_name = 'accordion'

urlpatterns = [
    path('', views.index, name='index'),
]