from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.selenium = webdriver.Chrome(
            './chromedriver',
            chrome_options=chrome_options
        )
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_can_post_status_and_appear_on_homepage(self):
        # Rona just discovered a new website that contains some personal information of her friend, Aldi
        # So she decided to give the website visit
        self.selenium.get(self.live_server_url + '/story8')
        self.selenium.implicitly_wait(10)

        # She looks at the title, it's Alcordion, it's a fusion from Aldi and Accordion she guessed
        self.assertIn("Alcordion", self.selenium.title)

        # She's amazed with what she sees
        # There are 5 accordions in the page (Rona is also a web developer but couldn't make accordion yet)
        accordions = self.selenium.find_elements_by_class_name('accordion')
        self.assertEqual(len(accordions), 4)

        # The top accordion is expanded, and Rona can sees Aldi's personal info,
        # such as his name, Aldi Naufal Fitrah
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertIn('Aldi Naufal Fitrah', page_text)

        # and his college name, Universitas Indonesia
        self.assertIn('Universitas Indonesia', page_text)

        # And now, she wants to take a look at Aldi's current activity
        # So she clicks at the activity's accordion bar
        accordion_bar_activity = self.selenium.find_element_by_xpath("//div[@class='accordion activity']/div[@class='accordion-bar']")
        accordion_bar_activity.click()
        time.sleep(0.4)

        # Surprisingly, the profile's accordion got collapsed
        # So she can't see Aldi's name anymore
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertNotIn('Aldi Naufal Fitrah', page_text)

        # Instead, she sees Aldi's current activities
        # Like nugas, stress, nugas lagi, ngerjain linebot, nugas terus, buat scele notifier
        self.assertIn('nugas', page_text)
        self.assertIn('stress', page_text)
        self.assertIn('nugas lagi', page_text)
        self.assertIn('ngerjain linebot', page_text)
        self.assertIn('nugas terus', page_text)
        self.assertIn('buat scele notifier', page_text)

        # Wow, that's awesome. Now she wants to look at Aldi's organizatonal experiences
        # She click at the organization's accordion bar
        accordion_bar_organizaiton = self.selenium.find_element_by_xpath("//div[@class='accordion organization']/div[@class='accordion-bar']")
        accordion_bar_organizaiton.click()
        time.sleep(0.4)

        # Just as before, the activity's accordion got collapsed
        # And the organiztion's accordion got expanded
        # So she can't see Aldi's activities anymore
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertNotIn('nugas', page_text)

        # Now she sees Aldi's experience like Pemira, BEM, and FUKI
        self.assertIn('Pemira Fasilkom', page_text)
        self.assertIn('BEM Fasilkom', page_text)
        self.assertIn('FUKI Fasilkom', page_text)

        # She also wants to check the last part of the accordion, the achievement part
        accordion_bar_achievement = self.selenium.find_element_by_xpath("//div[@class='accordion achievement']/div[@class='accordion-bar']")
        accordion_bar_achievement.click()
        time.sleep(0.4)

        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertNotIn('Pemira', page_text)

        self.assertIn('Juara', page_text)
        
        # Now, Rona is courious about 2 buttons on the app bar, ^ and v
        # She tries to click at the v button on profile's accordion bar
        profile_bar_down_button = self.selenium.find_element_by_xpath("//div[@class='accordion profile']//button[@class='down']")
        profile_bar_down_button.click()
        time.sleep(0.4)

        # Impressive, the profile accordion goes down
        # and the activity accordion is now above the profile
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Aktivitas') < page_text.find('Profil'))

        # She wants to click the button again to see what will happen
        profile_bar_down_button = self.selenium.find_element_by_xpath("//div[@class='accordion profile']//button[@class='down']")
        profile_bar_down_button.click()
        time.sleep(0.4)

        # Just as she expected, now the organization accordion is above the profile
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Organisasi') < page_text.find('Profil'))

        # So now, the order is activity, organization, profile, and achievement
        # What if I click the down button in activity
        activity_bar_down_button = self.selenium.find_element_by_xpath("//div[@class='accordion activity']//button[@class='down']")
        activity_bar_down_button.click()
        time.sleep(0.4)

        # Well, the activity accordion goes down
        # And the organization accordion is on top
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Organisasi') < page_text.find('Aktivitas'))

        # The order now is organization, activity, profile, achievement
        # Now what about the ^ button, we should try it
        # Let's try to make the achievement accordion to the top
        achievement_bar_up_button = self.selenium.find_element_by_xpath("//div[@class='accordion achievement']//button[@class='up']")
        achievement_bar_up_button.click()
        time.sleep(0.4)

        # Great, it goes above the profile accordion
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Prestasi') < page_text.find('Profil'))

        # Let's click it again, maybe twice
        achievement_bar_up_button = self.selenium.find_element_by_xpath("//div[@class='accordion achievement']//button[@class='up']")
        achievement_bar_up_button.click()
        achievement_bar_up_button.click()
        time.sleep(0.4)

        # Now it's on top, the order should be achievement, organization, activity, profile
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Prestasi') < page_text.find('Organisasi'))

        # Let's try the up button on organization too
        organization_bar_up_button = self.selenium.find_element_by_xpath("//div[@class='accordion organization']//button[@class='up']")
        organization_bar_up_button.click()
        time.sleep(0.4)

        # And now the organization accordion is back to the top
        page_text = self.selenium.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Organisasi') < page_text.find('Prestasi'))

        # Because of the challenge, now Aldi's site has a new feature
        # It allows visitor to change style from dark to light
        # By default, the bg-color is black and so the accordion content, but the accordion bar is in red
        bg_color = self.selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        accordion_bar_color = self.selenium.find_element_by_class_name('accordion-bar').value_of_css_property('background-color')
        accordion_content_color = self.selenium.find_element_by_class_name('accordion-content').value_of_css_property('background-color')

        # Rona tries to click the style-modifier button
        style_button = self.selenium.find_element_by_id('style-modifier')
        style_button.click()

        # Now the bg-colors have changed
        new_bg_color = self.selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        new_accordion_bar_color = self.selenium.find_element_by_class_name('accordion-bar').value_of_css_property('background-color')

        self.assertNotEqual(new_bg_color, bg_color)
        self.assertNotEqual(new_accordion_bar_color, accordion_bar_color)
